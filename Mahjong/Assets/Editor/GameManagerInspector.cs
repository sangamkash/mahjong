﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Mahjong.GamePlay.Inspector
{
    [CustomEditor(typeof(GameManager))]
    [CanEditMultipleObjects]
    public class GameManagerInspector : UnityEditor.Editor
    {
        private bool mFolded;
        public override void OnInspectorGUI()
        {
            mFolded=EditorGUILayout.Foldout(mFolded, "Matrix");
            if (mFolded)
            {
                var gm = (GameManager) target;
                if (gm.pGameMatrix != null)
                    DrawMatrix(gm.pGameMatrix, gm.pRow, gm.pColumn);
                else
                    GUILayout.Label("Matrix is not Assigned");

            }

            base.OnInspectorGUI();
        }

        private void DrawMatrix(int[,]matrix ,int row,int column)
        {
            for (int i = 0; i < row; i++)
            {
                EditorGUILayout.BeginHorizontal();
                {
                    for (int j = 0; j < column; j++)
                    {
                        var value = matrix[i, j];
                        GUILayout.Label($"({i},{j})={value.ToString()}");
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
            GUILayout.Space(10f);
        }
    }
}
