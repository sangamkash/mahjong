﻿using System.Collections;
using System.Collections.Generic;
using Mahjong.GamePlay;
using Mahjong.LevelSystem.data;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelInfo))]
public class LevelDataInspector : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        GUILayout.Label("Matrix is not Assigned");
        if(GUILayout.Button("ValidateData"))
        {
            var levelInfo = (LevelInfo) target;
            levelInfo.Validate();
        }
        base.OnInspectorGUI();
    }
}
