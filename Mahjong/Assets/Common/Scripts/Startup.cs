﻿using System.Collections;
using System.Collections.Generic;
using Mahjong.Contants;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Mahjong.InitiateStart
{
    public class Startup : MonoBehaviour
    {
        [SerializeField] private float m_LoadingTime = 5f;
        private void Start()
        {
            StartCoroutine(LoadSceneWithWait(m_LoadingTime));
        }

        private IEnumerator LoadSceneWithWait(float time)
        {
            yield return new WaitForSeconds(time);
            SceneManager.LoadScene(GameConstant.SCENE_LEVEL);
        }
    }
}
