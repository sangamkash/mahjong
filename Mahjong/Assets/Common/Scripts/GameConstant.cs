﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mahjong.Contants
{

    public static class GameConstant
    {
        //Player Prefbs name 
        public const string PP_SELECTED_LEVEL_INDEX = "SELECTED_LEVEL_INDEX";
        //SceneName
        private const string SCENE_STARTUP = "StartUp";
        public const string SCENE_LEVEL = "Level";
        public const string SCENNE_GAMEPLAY = "GamePlay";
        
    }
}
