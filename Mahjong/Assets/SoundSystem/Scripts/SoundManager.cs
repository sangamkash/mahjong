﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Mahjong.SoundSystem
{
    [Serializable]
    public enum SoundType
    {
        ButtonSound,
        TileClick,
        SuccessTileHit,
        WrongMove,
    }

    [Serializable]
    public class SoundRef
    {
        public SoundType _SoundType;
        public AudioClip _AudioClip;
    }
    public class SoundManager : MonoBehaviour
    {
        public static SoundManager pInstance { get;private set; }
        [Header("External Dependency")]
        [SerializeField] private SoundRef[] m_soundRefs;
        [SerializeField] private AudioSource m_audioPrefab;
        [SerializeField] private AudioClip m_BgSound;
        [SerializeField] private AudioSource m_BgAudioSource;
        private Dictionary<SoundType, AudioSource> mSoundSources;
        private bool mMuteAudio;
        private void Awake()
        {
            if (pInstance == null)
            {
                pInstance = this;
                DontDestroyOnLoad(gameObject);
                pInstance.Init();
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Init()
        {
            mSoundSources= new Dictionary<SoundType, AudioSource>();
            var thisTrans = transform;
            for (int i = 0; i < m_soundRefs.Length; i++)
            {
                var data = m_soundRefs[i];
                var gameObj=Instantiate(m_audioPrefab.gameObject,thisTrans);
                gameObj.name = data._SoundType.ToString();
                gameObj.transform.parent = thisTrans;
                var audioSource = gameObj.AddComponent<AudioSource>();
                audioSource.clip = data._AudioClip;
                mSoundSources.Add(data._SoundType,audioSource);
            }
        }
        public void PlaySoundType(SoundType soundType)
        {
            if(mMuteAudio)
                return;
            if (mSoundSources.ContainsKey(soundType))
            {
                var audioSource=mSoundSources[soundType];
                    if(audioSource.isPlaying)
                        audioSource.Stop();
                    audioSource.Play();
            }
            else
            {
#if DEBUG_LOG
                Debug.LogError($"TAG::SoundManager || Sound Type <color=red>{soundType.ToString()} </color> is not added to sound Manager");
#endif
            }
        }

        public void MuteAll()
        {
            mMuteAudio = true;
            foreach (var audioItem in mSoundSources)
            {
                var audioSource = audioItem.Value;
                if(audioSource.isPlaying)
                    audioSource.Stop();
            }
            m_BgAudioSource.Stop();
        }
    }
}
