﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mahjong.SoundSystem
{
    public class SoundComponent : MonoBehaviour
    {
        [SerializeField] private SoundType m_SoundType;
        public void PlaySound()
        {
            SoundManager.pInstance.PlaySoundType(m_SoundType);
        }
    }
}
