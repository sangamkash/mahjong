﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using Mahjong.Contants;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Mahjong.Screen
{
    public class GameOverScreen : BaseScreen
    {

        [Header("Internal Dependency")] 
        [SerializeField] private Text m_MessageTxt;
        public void ShowMessage(string msg)
        {
            m_MessageTxt.text = msg;
            gameObject.SetActive(true);
        }
        public void BackButton()
        {
            SceneManager.LoadScene(GameConstant.SCENE_LEVEL);
        }

        public void RetryButton()
        {
            SceneManager.LoadScene(GameConstant.SCENNE_GAMEPLAY);
        }
    }
}
