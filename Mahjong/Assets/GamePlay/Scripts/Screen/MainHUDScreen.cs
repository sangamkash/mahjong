﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mahjong.Contants;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Mahjong.Screen
{
    public class MainHUDScreen : BaseScreen
    {
        [Header("InternalDependency")] 
        [SerializeField] private GameObject m_WrongGameObj;

        private Action<bool> mDisplayHitAction;
        public void Init(Action<bool> displayHitAction)
        {
            mDisplayHitAction = displayHitAction;
        }
        public void BackButton()
        {
            SceneManager.LoadScene(GameConstant.SCENE_LEVEL);
        }

        public void ShowHintButton()
        {
            mDisplayHitAction?.Invoke(true);
        }

        public void ShowWrongWarning()
        {
            StopAllCoroutines();
            m_WrongGameObj.SetActive(false);
            StartCoroutine(DisplayWrongMove());
        }
        
        private IEnumerator DisplayWrongMove()
        {
            m_WrongGameObj.SetActive(true);
            yield return new WaitForSeconds(2);
            m_WrongGameObj.SetActive(false);
        }
    }
}
