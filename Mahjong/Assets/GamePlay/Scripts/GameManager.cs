﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Mahjong.Contants;
using Mahjong.LevelSystem.data;
using UnityEngine;
using  Mahjong.Extension;
using Mahjong.GamePlay.Grid;
using Mahjong.Screen;
using Mahjong.SoundSystem;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Mahjong.GamePlay
{
    [System.Serializable]
    public enum AnimationState
    {
        Completed,
        Inprogress
    }
    public class GameManager : MonoBehaviour
    {
        public int[,] pGameMatrix { get; private set; }
        public  int pRow{ get; private set; }
        public  int pColumn{ get; private set; }
        private SoundManager pSoundManager=>SoundManager.pInstance;

        [Header("External Dependency")] 
        [SerializeField] private LevelInfo m_LevelInfo;
        [SerializeField] private TileGrid m_TileGrid;
        [SerializeField] private MainHUDScreen m_MainHudScreen;
        [SerializeField] private GameOverScreen m_GameOverScreen;
        
        [Header("Status")] 
        [SerializeField] private AnimationState m_AnimationState;
        [SerializeField] private Vector2Int m_lastCoordinate;
        
        private Dictionary<Vector2Int, Tile> mTileData;
        private Dictionary<Vector2Int, Tile> mActiveTileData;
        private Vector2Int tempCoordinate= Vector2Int.one*-1;
        private Coroutine mCoroutine;
        private List<Vector2Int> hints;
        private const string mSuccessfullyMsg = "You have successfully completed level";
        private const string mUnSuccessfullyMsg = "You failed to completed level";
        private string mLevelName;
        private void Start()
        {
            Init();
        }

        private void Init()
        {
            var index = PlayerPrefs.GetInt(GameConstant.PP_SELECTED_LEVEL_INDEX);
            var levelData = m_LevelInfo._LevelDatas[index];
            var levelJson = levelData._TextAsset.text;
            var tileTypeCount =levelData._TileCounts.Length;
            var tileCount = new int[tileTypeCount];
            for (int i = 0; i < tileTypeCount; i++)
            {
                tileCount[i] =levelData._TileCounts[i]._NoOfTile;
            }
            mLevelName = levelData._LevelName;
            var mSpriteData= new Sprite[tileTypeCount+1];
            mSpriteData[0] = null;
            for (int i = 1; i < mSpriteData.Length; i++)
            {
                mSpriteData[i] = levelData._TileCounts[i - 1]._Sprite;
            }
            var r = 0;
            var c = 0;
            pGameMatrix=levelJson.Parse(tileCount,out r,out c);
            pRow = r;
            pColumn = c;
            tempCoordinate= Vector2Int.one*-1;
            m_lastCoordinate = tempCoordinate;
            m_AnimationState = AnimationState.Completed;
            m_MainHudScreen.Init(DisplayHint);
#if DEBUG_LOG
            Debug.Log($"TAG:Gameplay || index:: {index} ");
            Debug.Log($"TAG:Gameplay || Tile count {tileCount}");
            Debug.Log($"TAG:Gameplay || string data:: {m_LevelInfo._LevelDatas[index]._TextAsset}");
            Debug.Log($"TAG:Gameplay || pGameMatrix:: {pGameMatrix == null} ");
#endif

            mTileData=new Dictionary<Vector2Int, Tile>();
            mActiveTileData= new Dictionary<Vector2Int, Tile>();
            hints=new List<Vector2Int>();
            m_TileGrid.PopulateTile(pGameMatrix,pRow,pColumn,ref mTileData,ref mActiveTileData,OnTilePress,mSpriteData);
            CheckGameOver();
        }

        private void OnTilePress(Vector2Int coordinate)
        {
            if (m_AnimationState == AnimationState.Inprogress)
            {
                //Ignore Input;
                return;
            }

            DisplayHint(false);
#if DEBUG_LOG
            Debug.Log($"TAG:Gameplay || coordinate:: {coordinate.ToString()} ");
#endif
            if (m_lastCoordinate == coordinate)
            {
                m_lastCoordinate = tempCoordinate;
                mTileData[coordinate].HighLight(false);
                return;
            }
            else
            {
                mTileData[coordinate].HighLight(true);
                if (m_lastCoordinate != tempCoordinate)
                {
                    CheckForHit(m_lastCoordinate, coordinate);
                    m_lastCoordinate = tempCoordinate;
                    return;
                }
            }

            m_lastCoordinate = coordinate;
        }

        private void CheckGameOver()
        {
            var value = true;
            var list = mActiveTileData.ToList();
            var count = list.Count;
            hints.Clear();
            var hintAdd = false;
            for (int i = 0; i < count; i++)
            {
                var start = list[i].Key;
                for (int j = i+1; j < count; j++)
                {
                    var end = list[j].Key;
                    var successPath= new List<Vector2Int>();
                   
                    if (pGameMatrix.IsValidMove(start, end, out successPath))
                    {
                        hints.Add(start);
                        hints.Add(end);
                        hintAdd = true;
                        break;
                    }
                }
                if(hintAdd)
                    break;
            }

            if (hints.Count <= 0 && mActiveTileData.Count <= 0)
            {
                //GameOver successfully 
                m_GameOverScreen.ShowMessage(mSuccessfullyMsg);
                PlayerPrefs.SetInt(mLevelName, 1);
                PlayerPrefs.Save();
            }
            else if (hints.Count <= 0 && mActiveTileData.Count > 0)
            {
                //GameOver unSuccessfully
                m_GameOverScreen.ShowMessage(mUnSuccessfullyMsg);
            
            }
        }

        private void DisplayHint(bool value)
        {
            if (m_AnimationState == AnimationState.Inprogress)
            {
                //Ignore Input;
                return;
            }
            var count = hints.Count;
            for (int i = 0; i < count; i++)
            {
                mTileData[hints[i]].ShowHint(value);
            }
        }
        
        private bool CheckForHit(Vector2Int start,Vector2Int end)
        {
            var successPath= new List<Vector2Int>();
            if (pGameMatrix.IsValidMove(start, end,out successPath))
            {
                pSoundManager.PlaySoundType(SoundType.SuccessTileHit);
                StartCoroutine(PlaySuccessAnimation(start, end, successPath));
                return true;
            }
            else
            {
                pSoundManager.PlaySoundType(SoundType.WrongMove);
                m_MainHudScreen.ShowWrongWarning();
                mTileData[start].HighLight(false);
                mTileData[end].HighLight(false);
                return false;
            }
        }
        private IEnumerator PlaySuccessAnimation(Vector2Int start,Vector2Int end,List<Vector2Int> successPath)
        {
            var m_successPath = successPath;
            m_AnimationState = AnimationState.Inprogress;
            var count = successPath.Count;
            for (var i = 0; i < count; i++)
            {
                mTileData[successPath[i]].DrawPath(true);
                yield return new WaitForSeconds(0.05f);
            }
            yield return new WaitForSeconds(0.5f);
            for (var i = 0; i < count; i++)
            {
                mTileData[successPath[i]].Hide();
            }
            pGameMatrix[start.x, start.y] = 0;
            pGameMatrix[end.x, end.y] = 0;
           
            mActiveTileData.Remove(start);
            mActiveTileData.Remove(end);
            CheckGameOver();
            m_AnimationState = AnimationState.Completed;
        }
    }
}
