﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Mahjong.GamePlay.Grid
{
    public class TileGrid : MonoBehaviour
    {
        [Header("External Dependency")] 
        [SerializeField] private Tile m_TilePrefab;
        
        [Header("Internal Dependency")] 
        [SerializeField] private RectTransform m_TileContainer;
        
        [Header("Config")]
        [Range(1f,2f)]
        [SerializeField] private float m_TileAspectRation = 1.3f;

        public void PopulateTile(int[,] pGameMatrix, int pRow, int pColumn, ref Dictionary<Vector2Int, Tile> mTileData,
            ref Dictionary<Vector2Int, Tile> mActiveTileData, Action<Vector2Int> OnTilePress, Sprite[] mSpriteData)
        {

            var rowBig = pRow * m_TileAspectRation > pColumn;
            var count = rowBig ? pRow * m_TileAspectRation : pColumn;
            var length = rowBig ? m_TileContainer.rect.height : m_TileContainer.rect.width;
            var grg = m_TileContainer.GetComponent<GridLayoutGroup>();
            var size = length / (count);
            if (rowBig)
            {
                var scale = (float)  m_TileContainer.rect.width /(pColumn*size);
                m_TileContainer.localScale = Vector3.one * Mathf.Clamp01(scale);
                grg.cellSize = new Vector2(size, size * m_TileAspectRation);
                grg.constraint = GridLayoutGroup.Constraint.FixedRowCount;
                grg.constraintCount = pRow;
            }
            else
            {
                var scale = (float)  m_TileContainer.rect.height /(pRow*size*m_TileAspectRation);
                m_TileContainer.localScale = Vector3.one * Mathf.Clamp01(scale);
                grg.cellSize = new Vector2(size, size * m_TileAspectRation);
                grg.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
                grg.constraintCount = pColumn;
            }

            mTileData = new Dictionary<Vector2Int, Tile>();
            for (var i = 0; i < pRow; i++)
            {
                for (var j = 0; j < pColumn; j++)
                {
                    var value = pGameMatrix[i, j];
                    var tile = Instantiate(m_TilePrefab.gameObject, m_TileContainer).GetComponent<Tile>();
                    var coordinate = new Vector2Int(i, j);
                    tile.Init(coordinate, OnTilePress, mSpriteData[value]);
                    if (value == 0)
                        tile.Hide();
                    else
                        mActiveTileData.Add(coordinate, tile);
                    mTileData.Add(coordinate, tile);
                }
            }
        }
    }
}
