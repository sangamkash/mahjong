﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mahjong.Contants;
using UnityEngine;
using UnityEngine.UI;

namespace Mahjong.GamePlay.Grid
{ 
    public class Tile : MonoBehaviour
    {
        [SerializeField] private GameObject m_container;
        [SerializeField] private GameObject m_HighLighthint;
        [SerializeField] private GameObject m_HighLight;
        [SerializeField] private GameObject m_PathHighLight;
        [SerializeField] private Image m_Image;
        [SerializeField] private Vector2Int m_Coordinate;
        [SerializeField] private Text m_Text;
        [SerializeField] private ParticleSystem m_PartilceSystem;
        private Action<Vector2Int> mOnTilePress;
        public bool pHidden { get; private set; }
        public void Init(Vector2Int coordinate, Action<Vector2Int> OnTilePress,Sprite sprite)
        {
            m_Coordinate = coordinate;
            mOnTilePress = OnTilePress;
            m_Image.sprite = sprite;
            m_Text.text = coordinate.ToString();
            pHidden = sprite == null;
        }

        public void Hide()
        {
            pHidden = true;
            m_container.SetActive(false);
            HighLight(false);
            DrawPath(false);
        }

        public void HighLight(bool value)
        {
            m_HighLight.SetActive(value);
        }

        public void ShowHint(bool value)
        {
            m_HighLighthint.SetActive(value);
            if(value)
            m_PartilceSystem.Play();
        }

        public void OnButtonPressed()
        {
            mOnTilePress?.Invoke(m_Coordinate);
        }

        public void DrawPath(bool value)
        {
            m_PathHighLight.SetActive(value);
            if (value)
            {
                m_PartilceSystem.Stop();
                m_PartilceSystem.Play();
            }
        }
        
    }
}
