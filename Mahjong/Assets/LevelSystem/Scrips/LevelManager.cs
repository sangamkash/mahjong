﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mahjong.Contants;
using Mahjong.LevelSystem.data;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Mahjong.LevelSystem
{
    public class LevelManager : MonoBehaviour
    {
        [Header("External Refs")]
        [SerializeField] private GameObject m_LevelBtnPrefab;
        [SerializeField] private LevelInfo m_LevelInfo;
        [SerializeField] private Sprite m_CompeletedBtnSprite;
        [SerializeField] private Sprite m_UnCompeletedBtnSprite;
        [Header("Internal Ref")]
        [SerializeField] private RectTransform m_LevelBtnContainer;

        [SerializeField] private int m_NoOfLevelInRow=5;

        private bool mLoadingInProgress;
        
        private void Start()
        {
            Init();
        }

        private void Init()
        {
            mLoadingInProgress = false;
            var grg = m_LevelBtnContainer.GetComponent<GridLayoutGroup>();
            var size = m_LevelBtnContainer.rect.width / m_NoOfLevelInRow;
            grg.cellSize= new Vector2(size,size);
            for (int i = 0; i < m_LevelInfo._LevelDatas.Length; i++)
            {
                var levelData = m_LevelInfo._LevelDatas[i];
                var btn = Instantiate(m_LevelBtnPrefab, m_LevelBtnContainer).GetComponent<Button>();
                btn.transform.GetChild(0).GetComponent<Text>().text = levelData._LevelName;
                var index = i;
                btn.onClick.AddListener(() =>LoadLevel(index));
                if (isLevelCompleted(levelData._LevelName))
                {
                    btn.image.sprite = m_CompeletedBtnSprite;
                }
                else
                {
                    btn.image.sprite = m_UnCompeletedBtnSprite;
                }
            }
            
        }

        //TODO we can save this as .Dat file 
        private bool isLevelCompleted(string levelName)
        {
            if (PlayerPrefs.HasKey(levelName))
            {
                return PlayerPrefs.GetInt(levelName) > 0;
            }
            return false;
        }


        private void LoadLevel(int levelIndex)
        {
            if (mLoadingInProgress == true)
                return;
            mLoadingInProgress = true;
            PlayerPrefs.SetInt(GameConstant.PP_SELECTED_LEVEL_INDEX, levelIndex);
            PlayerPrefs.Save();
            SceneManager.LoadScene(GameConstant.SCENNE_GAMEPLAY);
        }

    }
}
