﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mahjong.Extension;
using UnityEngine;

namespace Mahjong.LevelSystem.data
{
    [Serializable]
    public class TileCounts
    {
        public Sprite _Sprite;
        public int _NoOfTile;
    }
    [Serializable]
    public class LevelData
    {
        public string _LevelName;
        //TODO can be done in better way by loading it from resource 
        public TextAsset _TextAsset;
        public TileCounts[] _TileCounts;
    }
    [CreateAssetMenu(fileName = "LevelData", menuName = "Mahjong/Data", order = 1)]
    public class LevelInfo : ScriptableObject
    {
        public LevelData[] _LevelDatas;
       
#if UNITY_EDITOR
        public void Validate()
        {
            for(int i=0;i<_LevelDatas.Length;i++)
            {
                var data = _LevelDatas[i];
                var maxNoOfTile = data._TextAsset.text.GetNumberOfTile();
                if (maxNoOfTile % 2 != 0)
                {
                    Debug.LogError($"For levelName <color=blue>{data._LevelName}</color>  <color=red>X</color> should even number");
                    return;
                }

                var sum = 0;
                if (data._TileCounts.Length <= 0)
                {
                    Debug.LogError($"For levelName <color=blue>{data._LevelName}</color> " +
                                   $" <color=red>_TileCounts</color> should not be <color=red>0</color>");
                }
                for (int j = 0; j < data._TileCounts.Length; j++)
                {
                    var tileCount = data._TileCounts[j];
                    sum += tileCount._NoOfTile;
                    if (tileCount._NoOfTile % 2 != 0)
                    {
                        Debug.LogError($"For levelName <color=blue>{data._LevelName}</color> " +
                                       $" <color=red>_NoOfTile</color> should even number ");
                        return;
                    }
                }

                if (sum != maxNoOfTile)
                {
                    Debug.LogError($"For levelName <color=blue>{data._LevelName}</color>  " +
                                   $"<color=red>_NoOfTile</color> should be equal to number of <color=red>X</color> = {maxNoOfTile}");
                    return;
                }
            }
            Debug.Log("<color=green>All Clear no error found </color>");

        }
#endif
    }
}
