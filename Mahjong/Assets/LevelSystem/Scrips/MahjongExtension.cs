﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEditor;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Mahjong.Extension
{
    public static class MahjongExtension
    {
        public static int[,] Parse(this string data, int numberOfTile, out int  r,out int  c)
        {
            var rows = data.Split('\n');
            r = rows.Length;
            c = rows[0].Trim().ToCharArray().Length;
            var matrix = new int[r, c];
            for (int i = 0; i < r; i++)
            {
                for (int j = 0; j <c; j++)
                {
                    switch (rows[i].ToCharArray()[j])
                    {
                        case '0':
                            matrix[i, j] = 0;
                            break;
                        case 'X':
                            matrix[i, j] = Random.Range(1, numberOfTile + 1);
                            break;
                    }
                }
            }
            return matrix;
        }
        
        public static int GetNumberOfTile(this string data)
        {
            var rows = data.Split('\n');
            var r = rows.Length;
            var c = rows[0].Trim().ToCharArray().Length;
            var numberOfTiles = 0;
            for (int i = 0; i < r; i++)
            {
                for (int j = 0; j < c; j++)
                {
                    switch (rows[i].ToCharArray()[j])
                    {
                        case '0':
                            break;
                        case 'X':
                            numberOfTiles++;
                            break;
                    }
                }
            }
            return numberOfTiles;
        }
        
        public static int[,] Parse(this string data, int[] tileTypesCount, out int  r,out int  c)
        {
            
            var rows = data.Split('\n');
            r = rows.Length;
            c = rows[0].Trim().ToCharArray().Length;
            var maxNumber = 0;
            var matrix = new int[r, c];
            for (int i = 0; i <r; i++)
            {
                for (int j = 0; j <c; j++)
                {
                    switch (rows[i].ToCharArray()[j])
                    {
                        case '0':
                            matrix[i, j] = 0;
                            break;
                        case 'X':
                            matrix[i, j] = 1;
                            maxNumber++;
                            break;
                    }
                }
            }
            Dictionary<int,int> currentStatus= new Dictionary<int, int>();
            for (int i = 0; i <r; i++)
            {
                for (int j = 0; j < c; j++)
                {
                    switch (matrix[i,j])
                    {
                        case 1:
                            matrix[i, j] = ReturnWeightedNumber(tileTypesCount,ref currentStatus,maxNumber); 
                            break;
                    }
                }
            }
            return matrix;
        }

        private static int ReturnWeightedNumber(int[] tileTypesCount,ref Dictionary<int,int> currentStatus,int maxNumber)
        {
            List<int> randomTileList=new List<int>();
            for (int i = 0; i < tileTypesCount.Length; i++)
            {
                var count = 0;
                var tileType = i;
                var maxTileCount = tileTypesCount[tileType];
                if (currentStatus.ContainsKey(tileType))
                    count = currentStatus[tileType];
                if(count<maxTileCount)
                    randomTileList.Add(tileType);
            }
            var result=0;
            var listCount = randomTileList.Count;
            if (listCount > 0)
                result = randomTileList[Random.Range(0, listCount)];
          
            if (currentStatus.ContainsKey(result))
            {
                var resultCount = currentStatus[result];
                currentStatus[result] = resultCount + 1;
            }
            else
            {
                currentStatus.Add(result,1);
            }

            
            return result + 1;
        }
        
        public static bool IsValidMove(this int[,] matrix, Vector2Int start, Vector2Int end,out List<Vector2Int> successPath)
        {
            successPath = new List<Vector2Int>();
            successPath.Add(start);
            var result = false;
            if (matrix.ReturnValue(start) != matrix.ReturnValue(end))
            {
                result = false;
            }
            else if (start.x == end.x)
            {
                //Case 1 :In the same row
                if (matrix.CheckRowEmpty(start, end, false, false,ref successPath))
                {
                    result = true;
                }
            }
            else if( start.y== end.y)
            {
                //Case 2 :In the same column
                if (matrix.CheckColumnEmpty(start, end, false, false,ref successPath))
                {
                    result = true;
                }
            }
            else 
            {
                //case 3: left <-> right then botton <-> up (bi-direction case 1) 
                var intermediatePath1=new List<Vector2Int>();
                var intermediatePath2=new List<Vector2Int>();
                if (matrix.CheckRowEmpty(start, new Vector2Int(start.x, end.y), false, true, ref intermediatePath1) &&
                    matrix.CheckColumnEmpty(new Vector2Int(start.x, end.y), end, false, false, ref intermediatePath1))
                {
                    result = true;
                    var count = intermediatePath1.Count;
                    for (var i = 0; i < count; i++)
                    {
                        successPath.Add(intermediatePath1[i]);
                    }
                }
                //case 3: left <-> right then botton <-> up (bi-direction case 2) 
                else if (matrix.CheckRowEmpty(end, new Vector2Int(end.x, start.y), false, true, ref intermediatePath2) &&
                         matrix.CheckColumnEmpty(new Vector2Int(end.x, start.y), start, false, false, ref intermediatePath2))
                {
                    result = true;
                    var count = intermediatePath2.Count;
                    for (var i = count-1; i >=0 ; i--)
                    {
                        successPath.Add(intermediatePath2[i]);
                    }
                }


            }
            successPath.Add(end);
            return result;
        }

        public static bool CheckRowEmpty(this int[,] matrix, Vector2Int start, Vector2Int end,bool startInclusive,bool endInclusive,ref List<Vector2Int> successPath)
        {
            //Debug.Log($" row empty start:{start}  end{end} se{startInclusive} ee{endInclusive}");
            var dir = 1;
            if (start.y > end.y)
            {
                dir = -1;
            }

            var row = start.x;
            var startC = startInclusive ? start.y : start.y + dir;
            var endC = endInclusive ? end.y + dir : end.y;
            var counter = 0;
            var traversedTile = new List<Vector2Int>();
            while (startC+counter != endC)
            {
                var pos=new Vector2Int(row,startC+counter);
                //Debug.Log("Row Empty::"+pos.ToString());
                traversedTile.Add(pos);
                if(matrix.ReturnValue(pos) != 0)
                    return false;
                counter += dir;
                if (counter > 1000000)
                {
                    Debug.LogError("Infinite Loop error");
                    return false;
                }
            }

            var count = traversedTile.Count;
            for (var i = 0; i < count; i++)
            {
                successPath.Add(traversedTile[i]);
            }
            return true;
        }
        
        public static bool CheckColumnEmpty(this int[,] matrix, Vector2Int start, Vector2Int end,bool startInclusive,bool endInclusive,ref List<Vector2Int> successPath)
        {
            //Debug.Log($" col empty start:{start}  end{end} se{startInclusive} ee{endInclusive}");

            var dir = 1;
            if (start.x > end.x)
            {
                dir = -1;
            }

            var col = start.y;
            var startR = startInclusive ? start.x : start.x + dir;
            var endR = endInclusive ? end.x + dir : end.x;
            var counter = 0;
            var traversedTile = new List<Vector2Int>();
            while (startR+counter != endR)
            {
                var pos = new Vector2Int(startR + counter, col);
                //Debug.Log("Col Empty::"+pos.ToString());
                traversedTile.Add(pos);
                if(matrix.ReturnValue(pos) != 0)
                    return false;
                counter += dir;
                if (counter > 1000000)
                {
                    Debug.LogError("Infinite Loop error");
                    return false;
                }
            }
            var count = traversedTile.Count;
            for (var i = 0; i < count; i++)
            {
                successPath.Add(traversedTile[i]);
            }
            return true;
        }

        private static int ReturnValue(this int[,] matrix, Vector2Int coordinate)
        {
            return matrix[coordinate.x, coordinate.y];
        }
      }
}
