**Naming convention**

- Public variable:
  - Should start with "_" as prefix
    - ex:   
      - public int _VariableName

- Private variable
  - Should start with "m" as prefix
    - ex:   
      - private int mVariableName

- Private [SerializeField] variable
  - Should start with "m_" as prefix
    - ex:   
      - [SerializeField] private int m_VariableName

- all type of property variable
  - Should start with "p" as prefix
    - ex:   
        - public int pVariableName{get;set;}
        - private int pVariableName{get;set;}
        - [SerializeField] private int pVariableName{get;set;}
        


**Games Description: **

This is a basic game of** Mahjong** ,where the aim of the player is to break the matching tile with two or less number of line drawn in the git 



